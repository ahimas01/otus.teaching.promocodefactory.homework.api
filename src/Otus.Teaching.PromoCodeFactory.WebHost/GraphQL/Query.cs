using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Query
    {
        public IQueryable<Customer> GetQueryCustomers([FromServices] DataContext context) => context.Customers;

        public async Task<CustomerResponse> GetCustomerAsync(Guid id,
            [FromServices] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);
            return response;
        }

        public async Task<List<CustomerShortResponse>> GetCustomersAsync(
            [FromServices] IRepository<Customer> customerRepository)
        {
            var customers = await customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return response;
        }
    }
}