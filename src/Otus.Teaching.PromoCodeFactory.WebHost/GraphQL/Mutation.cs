using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Mutation
    {
        public async Task<CustomerResponse> CreateCustomerAsync(
            CreateOrEditCustomerRequest input,
            [FromServices] IRepository<Customer> customerRepository,
            [FromServices] IRepository<Preference> preferenceRepository)
        {
            var preferences = await preferenceRepository
                .GetRangeByIdsAsync(input.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(input, preferences);

            await customerRepository.AddAsync(customer);
            var newCustomer = await customerRepository.GetByIdAsync(customer.Id);

            var response = new CustomerResponse(newCustomer);
            return response;
        }

        public async Task<int> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request,
            [FromServices] IRepository<Customer> customerRepository,
            [FromServices] IRepository<Preference> preferenceRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return 404;

            var preferences = await preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            return 204;
        }

        public async Task<int> DeleteCustomerAsync(Guid id, [FromServices] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return 404;

            await customerRepository.DeleteAsync(customer);

            return 204;
        }
    }
}