using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, ILogger<CustomerService> logger)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _logger = logger;
        }

        public override async Task<ArrayCustomerShortResponse> GetCustomersAsync(GetCustomersParams request,
            ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponseProto
            {
                Id = new GuidProto {Guid = x.Id.ToString()},
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return new ArrayCustomerShortResponse {Response = {response}};
        }

        public override async Task<CustomerResponseProto> GetCustomerAsync(GuidProto request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(TransformProtoGuid(request));
            return ToProtoCustomer(customer);
        }

        public override async Task<CustomerResponseProto> CreateCustomerAsync(CreateOrEditCustomerRequestProto request,
            ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(TransformProtoGuid).ToList());

            var customer = CustomerMapper.MapFromModel(new CreateOrEditCustomerRequest
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            }, preferences);

            await _customerRepository.AddAsync(customer);
            return ToProtoCustomer(customer);
        }

        public override async Task<StatusProto> EditCustomersAsync(CreateOrEditCustomerRequestWithId request,
            ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(TransformProtoGuid(request.Id));

            if (customer == null)
                return new StatusProto {Status = 404};

            var preferences =
                await _preferenceRepository.GetRangeByIdsAsync(request.Req.PreferenceIds.Select(TransformProtoGuid)
                    .ToList());

            CustomerMapper.MapFromModel(new CreateOrEditCustomerRequest
            {
                Email = request.Req.Email,
                FirstName = request.Req.FirstName,
                LastName = request.Req.LastName
            }, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new StatusProto {Status = 204};
        }

        public override async Task<StatusProto> DeleteCustomerAsync(GuidProto request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(TransformProtoGuid(request));

            if (customer == null)
                return new StatusProto {Status = 404};

            await _customerRepository.DeleteAsync(customer);

            return new StatusProto {Status = 204};
        }

        private CustomerResponseProto ToProtoCustomer(Customer customer)
        {
            var response = new CustomerResponseProto
            {
                Email = customer.Email,
                Id = new GuidProto {Guid = customer.Id.ToString()},
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences =
                {
                    customer.Preferences.Select(s => new PreferenceResponseProto
                    {
                        Id = new GuidProto {Guid = s.PreferenceId.ToString()},
                        Name = s.Preference?.Name
                    })
                }
            };
            return response;
        }

        private Guid TransformProtoGuid(GuidProto proto)
        {
            if (!Guid.TryParse(proto.Guid, out var guid))
                throw new ArgumentException("Неверный формат guid", "request");
            return guid;
        }
    }
}